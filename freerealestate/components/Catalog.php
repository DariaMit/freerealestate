<?php namespace Daria\FreeRealEstate\Components;

use Cms\Classes\ComponentBase;
use Daria\FreeRealEstate\Models\Estate;
use Daria\FreeRealEstate\Models\Value;

/**
 * Catalog Component
 */
class Catalog extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Catalog Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->page['objects'] = Estate::all();

//        $visits = $visitsQuery->paginate(10);
    }
}
