<?php namespace Daria\FreeRealEstate\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateTopicsTable Migration
 */
class CreateTopicsTable extends Migration
{
    public function up()
    {
        Schema::create('daria_freerealestate_topics', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->default(0);
            $table->string('theme');
            $table->string('text');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('daria_freerealestate_topics');
    }
}
