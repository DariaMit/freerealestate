<?php namespace Daria\FreeRealEstate\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateApplicationsTable Migration
 */
class CreateApplicationsTable extends Migration
{
    public function up()
    {
        Schema::create('daria_freerealestate_applications', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('username')->default(0);
            $table->unsignedBigInteger('estate_id')->default(0);
            $table->string('connection_type');
            $table->string('comment');
            $table->string('status')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('daria_freerealestate_applications');
    }
}
