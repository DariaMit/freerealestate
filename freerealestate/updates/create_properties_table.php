<?php namespace Daria\FreeRealEstate\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreatePropertiesTable Migration
 */
class CreatePropertiesTable extends Migration
{
    public function up()
    {
        Schema::create('daria_freerealestate_properties', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('daria_freerealestate_properties');
    }
}
