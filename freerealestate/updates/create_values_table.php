<?php namespace Daria\FreeRealEstate\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateValuesTable Migration
 */
class CreateValuesTable extends Migration
{
    public function up()
    {
        Schema::create('daria_freerealestate_values', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('property_id');
            $table->unsignedBigInteger('estate_id');
            $table->string('value');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('daria_freerealestate_values');
    }
}
