<?php namespace Daria\FreeRealEstate\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateEstatesTable Migration
 */
class CreateEstatesTable extends Migration
{
    public function up()
    {
        Schema::create('daria_freerealestate_estates', function (Blueprint $table) {
            $table->id();
            $table->string('address');
            $table->string('cadastral_number');
            $table->tinyInteger('status')->default(0);
            $table->decimal('price', 15, 2)->default(0);
            $table->decimal('area', 15, 2)->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('daria_freerealestate_estates');
    }
}
