<?php
//
//Route::get('/api/objects', function () {
//    return [
//        'params' => request()->all(),
//        'data' => [
//            [
//                'id' => 1,
//                'name' => 'Хата'
//            ],
//            [
//                'id' => 2,
//                'name' => 'Дом'
//            ]
//        ]
//    ];
//});
//
//Route::post('/api/objects', function () {
//    return [
//        'params' => request()->all(),
//        'data' => [
//            [
//                'id' => 1,
//                'name' => 'Хата'
//            ],
//            [
//                'id' => 2,
//                'name' => 'Дом'
//            ]
//        ]
//    ];
//});

use \Daria\FreeRealEstate\Http\Controllers\ApplicationController;
use \Daria\FreeRealEstate\Http\Controllers\EstateController;
use \Daria\FreeRealEstate\Http\Controllers\UserController;
use \Daria\FreeRealEstate\Http\Controllers\ChatController;

Route::group(['middleware' => 'web'], function () {
    Route::post('leave_application', [ApplicationController::class, 'leaveApplication']);
    Route::get('catalog', [EstateController::class, 'show']);
    Route::get('estate_object', [EstateController::class, 'showObject']);
//    Route::get('chat', [ApplicationController::class, 'showChat']);
    Route::get('profile', [UserController::class, 'profile']);
    Route::post('register', [UserController::class, 'register']);
    Route::post('login', [UserController::class, 'login']);
    Route::get('logout', [UserController::class, 'logout']);
    Route::post('profile/start_chat', [ChatController::class, 'start_chat']);
    Route::post('profile/send_message', [ChatController::class, 'send_message']);
    Route::get('profile/show_topics', [ChatController::class, 'show_topics']);
});
