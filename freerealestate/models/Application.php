<?php namespace Daria\FreeRealEstate\Models;

use Model;
use RainLab\User\Facades\Auth;
use RainLab\User\Models\User;

/**
 * Application Model
 */
class Application extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string table associated with the model
     */
    public $table = 'daria_freerealestate_applications';

    const STATUSES = [
        0 => 'Заявка отправлена',
        1 => 'Заявка обработана',
        2 => 'Заявка закрыта'
    ];

    const CONNECTION_TYPES = [
        0 => 'Telegram',
        1 => 'Звонок на мобильный',
        2 => 'По почте'
    ];

    /**
     * @var array guarded attributes aren't mass assignable
     */
    protected $guarded = ['id'];

    /**
     * @var array fillable attributes are mass assignable
     */
    protected $fillable = [
        'user_id',
        'estate_id',
        'connection_type',
        'comment',
        'status',
        'username',
        'phone',
        'email'
    ];

    /**
     * @var array rules for validation
     */
    public $rules = [
        'connection_type' => 'string|required',
//        'username' => 'required_without:user_id|string',
        'status' => 'numeric|required',
        'estate_id' => 'required|numeric',
        'phone' => 'required_if:connection_type,0,1',
        'email' => 'required_if:connection_type,2|email'
    ];

    public $customMessages = [
        'estate_id.required' => 'Выберите объект недвижимости',
//        'username.required_without:user_id' => 'Если пользователь незарегистрирован, введите его имя',
        'phone.required_if' => 'Если выбранный вид связи - телеграм или телефон, введите номер телефона',
        'email.required_if' => 'Если выбранный вид связи - почта, введите e-mail'

//        'username.required' => 'Введите Ваше имя'
    ];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array jsonable attribute names that are json encoded and decoded from the database
     */
    protected $jsonable = [];

    /**
     * @var array appends attributes to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array hidden attributes removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [
        'updated_at'
    ];

    /**
     * @var array dates attributes that should be mutated to dates
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array hasOne and other relations
     */

    public $hasMany = [
        'comments' => SupportComment::class
    ];
    public $belongsTo = [
        'user' => User::class,
        'estate' => Estate::class
    ];

//    public function getEmailAttribute($email){
//        Auth::registerGuest(['email' => $email]);
//    }

    public function getStatusOptions() {
        return self::STATUSES;
    }

    public function getConnectionTypeOptions() {
        return self::CONNECTION_TYPES;
    }

}
