<?php namespace Daria\FreeRealEstate\Models;

use Model;
use RainLab\User\Models\User;

/**
 * Topic Model
 */
class Topic extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string table associated with the model
     */
    public $table = 'daria_freerealestate_topics';

    /**
     * @var array guarded attributes aren't mass assignable
     */
    protected $guarded = ['*'];

    /**
     * @var array fillable attributes are mass assignable
     */
    protected $fillable = [
        'user_id',
        'theme',
        'text'
    ];

    /**
     * @var array rules for validation
     */
    public $rules = [
        'theme' => 'required|string|max:100',
        'text' => 'required|string|max:500'
    ];

    public $customMessages = [
        'theme.max:100' => 'Тема не должна превышать 100 знаков',
        'text.max:500' => 'Сообщение не должно превышать 500 знаков'
    ];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array jsonable attribute names that are json encoded and decoded from the database
     */
    protected $jsonable = [];

    /**
     * @var array appends attributes to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array hidden attributes removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [
        'updated_at'
    ];

    /**
     * @var array dates attributes that should be mutated to dates
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array hasOne and other relations
     */
    public $hasOne = [];
    public $hasMany = [
        'messages' => Message::class
    ];
    public $belongsTo = [
        'user' => User::class
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
