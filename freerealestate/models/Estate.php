<?php namespace Daria\FreeRealEstate\Models;

use Model;

/**
 * Estate Model
 */
class Estate extends Model
{
    use \October\Rain\Database\Traits\Validation;

    const AVAILABLE_SORT = [
        'name|asc',
        'name|desc',
        'price|asc',
        'price|desc'
    ];

    const STATUSES = [
        '0' => 'В продаже',
        '1' => 'Забронировано',
        '2' => 'Куплено'
    ];

    /**
     * @var string table associated with the model
     */
    public $table = 'daria_freerealestate_estates';

    /**
     * @var array guarded attributes aren't mass assignable
     */
    protected $guarded = ['*'];

    /**
     * @var array fillable attributes are mass assignable
     */
    protected $fillable = [];

    /**
     * @var array rules for validation
     */
    public $rules = [
        'address' => 'string|required',
        'cadastral_number' => 'string|required',
        'status' => 'numeric|required',
        'area' => 'numeric|required',
        'price' => 'numeric|required'
    ];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array jsonable attribute names that are json encoded and decoded from the database
     */
    protected $jsonable = [];

    /**
     * @var array appends attributes to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array hidden attributes removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array dates attributes that should be mutated to dates
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array hasOne and other relations
     */
//    public $hasOne = [];
    public $hasMany = [
        'values' => Value::class
    ];


//    public $belongsTo = [];
//    public $belongsToMany = [];
//    public $morphTo = [];
//    public $morphOne = [];
//    public $morphMany = [];
//    public $attachOne = [];
//    public $attachMany = [];

}
