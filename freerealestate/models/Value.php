<?php namespace Daria\FreeRealEstate\Models;

use Model;

/**
 * Value Model
 */
class Value extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string table associated with the model
     */
    public $table = 'daria_freerealestate_values';

    /**
     * @var array guarded attributes aren't mass assignable
     */
    protected $guarded = ['*'];

    /**
     * @var array fillable attributes are mass assignable
     */
    protected $fillable = [];

    /**
     * @var array rules for validation
     */
    public $rules = [
        'property_id' => 'required|numeric',
        'estate_id' => 'required|numeric',
        'value' => 'required|string|max:200'
    ];

    public $customMessages = [
        'value.max:200' => 'Значение свойства не должно превышать 200 знаков'
    ];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array jsonable attribute names that are json encoded and decoded from the database
     */
    protected $jsonable = [];

    /**
     * @var array appends attributes to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array hidden attributes removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [
        'estate_id',
        'created_at',
        'updated_at',
        'id'
    ];

    /**
     * @var array dates attributes that should be mutated to dates
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array hasOne and other relations
     */
//    public $hasOne = [
//        'property' => Property::class,
//        'estate' => Estate::class
//    ];
//    public $hasMany = [];
    public $belongsTo = [
        'property' => Property::class,
        'estate' => Estate::class
    ];

    public function getPropertiesOptions(): array
    {
        return Property::pluck('name')->all();
    }
//    public $belongsToMany = [];
//    public $morphTo = [];
//    public $morphOne = [];
//    public $morphMany = [];
//    public $attachOne = [];
//    public $attachMany = [];
}
