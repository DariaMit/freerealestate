<?php

namespace Daria\FreeRealEstate\Http\Controllers;

use Daria\FreeRealEstate\Http\Requests\Application\LeaveApplicationRequest;
use Daria\FreeRealEstate\Http\Requests\Application\SendMessageRequest;
use Daria\FreeRealEstate\Http\Requests\Application\ShowChatRequest;
use Daria\FreeRealEstate\Models\Application;
use Daria\FreeRealEstate\Models\Estate;
use Daria\FreeRealEstate\Models\SupportComment;
use Flash;
use Illuminate\Routing\Controller;
use RainLab\User\Facades\Auth;
use RainLab\User\Models\User;

class ApplicationController extends Controller
{
    public function leaveApplication(LeaveApplicationRequest $request)
    {

        $comment = e($request->get('comment'));
        $connectionType = e($request->get('connection_type'));
        $estateId = e($request->get('estate_id'));
        $userId = null;
        $phone = null;
        if ($request->has('phone')){
            $phone = e($request->get('phone'));
        }

        $estate = Estate::find($estateId);
        if (empty($estate)) {
            return response()->json(['message' => 'Мы не нашли этот обьект недвижимости'])->setStatusCode(406);
        }

        if (Auth::user()) {
            $userId = Auth::user()->id;
            $username = Auth::user()->name;
            $email = Auth::user()->email;
        } else {
            $request->validate([
                'email' => 'required|email',
                'username' => 'required|string'
            ]);
            $email = e($request->get('email'));
            $username = e($request->get('username'));
            If (User::where('email', $email)->exists()) {
                $registeredUser = User::where('email', $email)->first();
                $userId = $registeredUser->id;
                } else {
                $user = Auth::registerGuest(['email' => $email]);
                $userId = $user->id;
            }
        }

        $application = Application::create([
            'username' => $username,
            'user_id' => $userId,
            'estate_id' => $estateId,
            'status' => 0,
            'comment' => $comment,
            'connection_type' => $connectionType,
            'email' => $email,
            'phone' => $phone,
        ]);

        Flash::success('Settings successfully saved!');

        return $application;
    }

//    public function showChat(ShowChatRequest $request)
//    {
//        $topicId = e($request->get('application_id'));
//        $topic = Application::find($topicId);
//        if (!Auth::user() or $topic['user_id']!==Auth::user()->id){
//            return response()->json(['message' => 'У вас нет доступа к этому диалогу'])->setStatusCode(403);
//        }
//        $messages = SupportComment::where('application_id', $topicId)->get();
//        $topic['messages'] = $messages;
//        return $topic;
//    }

//    public function sendMessage(SendMessageRequest $request)
//    {
//        $topicId = e($request->get('application_id'));
//        $text = e($request->get('message'));
//        $userId = 0;
//        if (Auth::user()){
//                $userId = Auth::user()->id;
//            }
//        $topic = Application::find($topicId);
//        if (!Auth::user() or $topic['user_id']!==Auth::user()->id){
//            return response()->json(['message' => 'У вас нет доступа к этому диалогу'])->setStatusCode(403);
//        }
//        $message = SupportComment::create([
//            'user_id' => $userId,
//            'comment' => $text,
//            'application_id' =>$topicId
//        ]);
//            return $message;
//    }
}
