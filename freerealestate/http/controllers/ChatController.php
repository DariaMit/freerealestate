<?php namespace Daria\FreeRealEstate\Http\Controllers;

use Daria\FreeRealEstate\Http\Requests\Chat\SendMessageRequest;
use Daria\FreeRealEstate\Http\Requests\Chat\StartChatRequest;
use Daria\FreeRealEstate\Models\Message;
use Daria\FreeRealEstate\Models\Topic;
use Illuminate\Routing\Controller;
use Mail;
use RainLab\User\Facades\Auth;
//use Mailgun;

class ChatController extends Controller
{
    public function start_chat(StartChatRequest $request)
    {
        $userId = Auth::user()->id;
        $theme = e($request->get('theme'));
        $text = e($request->get('text'));

        return Topic::create([
            'user_id' => $userId,
            'theme' => $theme,
            'text' => $text
            ]);
    }

    public function send_message(SendMessageRequest $request)
    {
//        require 'vendor/autoload.php';
//
//# Instantiate the client.
//        $mgClient = new Mailgun('d65fdf7b4d9380939e68042853d163c8-4dd50799-143c6f50');
//        $domain = "sandbox9009e6368f894a46a49243cd0cc804db.mailgun.org";
//        $result = $mgClient->sendMessage("$domain",
//            array('from'    => 'Mailgun Sandbox <postmaster@sandbox9009e6368f894a46a49243cd0cc804db.mailgun.org>',
//                'to'      => 'Daria Mitrofanova <dariushka.tyan@gmail.com>',
//                'subject' => 'Hello Daria Mitrofanova',
//                'text'    => 'Congratulations Daria Mitrofanova, you just sent an email with Mailgun!  You are truly awesome! '));


        $userId = Auth::user()->id;
        $theme = e($request->get('theme'));
        $text = e($request->get('text'));

        $topic = Topic::find($theme);

        if (empty($topic)) {
            return response()->json(['message' => 'Такой темы не существует'])->setStatusCode(406);
        }

        if (!Auth::check() or $topic->user_id !== Auth::user()->id){
            return response()->json(['message' => 'У вас нет доступа к этому диалогу'])->setStatusCode(403);
        }


        $vars = ['name' => Auth::user()->name, 'text' => $text];

        Mail::send('daria.freerealestate::mail.message', $vars, function($message) {

            $message->to('dariushka.tyan@gmail.com');
            $message->subject('Сообщение от клиента');
        });

        return Message::create([
            'user_id' => $userId,
            'topic_id' => $theme,
            'text' => $text
        ]);
    }

    public function show_topics()
    {
        $userId = Auth::user()->id;
        $topics = Topic::with('messages')->where('user_id', $userId)->get();
        return $topics;
    }
}
