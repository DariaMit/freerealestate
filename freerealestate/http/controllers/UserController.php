<?php namespace Daria\FreeRealEstate\Http\Controllers;

use Daria\FreeRealEstate\Http\Requests\User\LoginRequest;
use Daria\FreeRealEstate\Http\Requests\User\RegisterRequest;
use Daria\FreeRealEstate\Http\Requests\Estate\ShowRequest;
use Daria\FreeRealEstate\Models\Estate;
use Event;
use Flash;
use Illuminate\Routing\Controller;
use Lang;
use RainLab\User\Facades\Auth;


class UserController extends Controller
{
    public function profile()
    {
        if (!Auth::check()){
            return response()->json(['message' => 'Вы не авторизованы'])->setStatusCode(403);
        }
        $user = Auth::getUser();
        $user['applications'] = $user->applications;
        return $user;

    }

    public function register(RegisterRequest $request)
    {
        $name = e(request()->get('name'));
        $email = e(request()->get('email'));
        $password = e(request()->get('password'));
        $passwordConfirmation = e(request()->get('password_confirmation'));

        $user = Auth::register([
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $passwordConfirmation,
        ], true);

        Auth::login($user);
        return response()->json(['message' => 'OK'])->setStatusCode(200);
    }

    public function login(LoginRequest $request)
    {
        $login = e($request->get('login'));
        $password = e($request->get('password'));

        return Auth::authenticate([
            'login' => $login,
            'password' => $password
        ]);
    }

    public function logout()
    {
        $user = Auth::getUser();
        Auth::logout();
        if ($user) {
            Event::fire('rainlab.user.logout', [$user]);
        }
        return response()->json(['message' => 'OK'])->setStatusCode(200);
    }
}
