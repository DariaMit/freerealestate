<?php namespace Daria\FreeRealEstate\Http\Controllers;

use Daria\FreeRealEstate\Http\Requests\Estate\ShowObjectRequest;
use Daria\FreeRealEstate\Http\Requests\Estate\ShowRequest;
use Daria\FreeRealEstate\Models\Estate;
use Illuminate\Routing\Controller;

class EstateController extends Controller
{
    public function show(ShowRequest $request)
    {
//        $data = $request->validated();

        $estateQuery = Estate::query();

        if ($request->has('sort') &&
            in_array($request->get('sort'), Estate::AVAILABLE_SORT)) {

            [$column, $direction] = explode('|', $request->get('sort'));
            $estateQuery = $estateQuery->orderBy($column, $direction);
        }

        $estateQuery = $estateQuery->paginate(5);

//        foreach ($estateQuery as $estate) {
//            $estate['status'] = Estate::STATUSES[$estate['status']];
//        }

        return $estateQuery;
    }

    public function showObject(ShowObjectRequest $request)
    {
        $estateId = e(request()->get('estate_id'));

        $estateQuery = Estate::with('values')->find($estateId);

        if (empty($estateQuery)) {
            return response()->json(['message' => 'Мы не нашли этот обьект недвижимости'])->setStatusCode(406);
        }

        return $estateQuery;
    }
}
