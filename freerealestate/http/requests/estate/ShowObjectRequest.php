<?php namespace Daria\FreeRealEstate\Http\Requests\Estate;

use Illuminate\Foundation\Http\FormRequest;

class ShowObjectRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'estate_id' => 'required|numeric',
        ];
    }
}
