<?php namespace Daria\FreeRealEstate\Http\Requests\Estate;

use Illuminate\Foundation\Http\FormRequest;

class ShowRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'sort' => 'sometimes|string',
        ];
    }
}
