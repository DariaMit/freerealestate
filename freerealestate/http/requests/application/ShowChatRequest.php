<?php namespace Daria\FreeRealEstate\Http\Requests\Application;

use Illuminate\Foundation\Http\FormRequest;

class ShowChatRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'application_id' => 'required|numeric'
        ];
    }
}
