<?php namespace Daria\FreeRealEstate\Http\Requests\Application;

use Illuminate\Foundation\Http\FormRequest;

class LeaveApplicationRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'comment' => 'sometimes|string',
            'estate_id' => 'required|numeric',
            'username' => 'sometimes|string',
            'connection_type' => 'required',
            'email' => 'sometimes|string',
            'phone' => 'sometimes|string'
        ];
    }
}
