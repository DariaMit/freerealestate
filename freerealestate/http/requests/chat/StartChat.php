<?php namespace Daria\FreeRealEstate\Http\Requests\Chat;

use Illuminate\Foundation\Http\FormRequest;

class StartChatRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'theme' => 'required|string|max:50',
            'text' => 'required|string'
        ];
    }
}
