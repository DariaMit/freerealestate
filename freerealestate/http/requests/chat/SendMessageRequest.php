<?php namespace Daria\FreeRealEstate\Http\Requests\Chat;

use Illuminate\Foundation\Http\FormRequest;

class SendMessageRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'theme' => 'numeric',
            'text' => 'required|string'
        ];
    }
}
