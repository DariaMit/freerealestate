<div data-control="toolbar">

<!--    --><?php //if($this->user->hasAccess('manage_properties')): ?>

        <a
            href="<?= Backend::url('daria/freerealestate/properties/create') ?>"
            class="btn btn-primary oc-icon-plus">
            <?= e(trans('backend::lang.list.create_button', ['name'=>'Property'])) ?>
        </a>

        <button
            class="btn btn-danger oc-icon-trash-o"
            disabled="disabled"
            onclick="$(this).data('request-data', { checked: $('.control-list').listWidget('getChecked') })"
            data-request="onDelete"
            data-request-confirm="<?= e(trans('backend::lang.list.delete_selected_confirm')) ?>"
            data-trigger-action="enable"
            data-trigger=".control-list input[type=checkbox]"
            data-trigger-condition="checked"
            data-request-success="$(this).prop('disabled', 'disabled')"
            data-stripe-load-indicator>
            <?= e(trans('backend::lang.list.delete_selected')) ?>
        </button>

<!--    --><?php //endif ?>
</div>
