<?php namespace Daria\FreeRealEstate\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Applications Backend Controller
 */
class Applications extends Controller
{
    public $implement = [
        \Backend\Behaviors\FormController::class,
        \Backend\Behaviors\ListController::class,
        \Backend\Behaviors\RelationController::class
    ];

    public $requiredPermissions = ['daria.freerealestate.access_applications'];

    /**
     * @var string formConfig file
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string listConfig file
     */
    public $listConfig = 'config_list.yaml';

    public $relationConfig = 'config_relation.yaml';

    /**
     * __construct the controller
     */
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Daria.FreeRealEstate', 'freerealestate', 'applications');
    }
}
