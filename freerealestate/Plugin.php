<?php namespace Daria\FreeRealEstate;

use Backend;
use Daria\FreeRealEstate\Models\Application;
use Daria\FreeRealEstate\Models\Profile;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use RainLab\Notify\Interfaces\Event;
use RainLab\User\Controllers\Users as UserController;
use RainLab\User\Models\User as UserModel;
use October\Rain\Exception\ErrorHandler;
use System\Classes\PluginBase;
use ValidationException;
use Exception;

/**
 * Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name' => 'FreeRealEstate',
            'description' => 'No description provided yet...',
            'author' => 'Daria',
            'icon' => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return void
     */
    public function boot()
    {
        UserModel::extend(function (UserModel $model) {
            $model->hasMany['applications'] = [Application::class];
            $model->setHidden(array_merge($model->getHidden(), [
                'created_at',
                'updated_at',
                'activated_at',
                'permissions',
                'is_activated',
                'username',
                'deleted_at',
                'last_seen',
                'last_login',
                'is_guest',
                'is_superuser',
                'created_ip_address',
                'last_ip_address'
            ]));
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [];
    }

    /**
     * Registers any backend permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {

        return [
            'daria.freerealestate.access_estate' => [
                'tab' => 'FreeRealEstate',
                'label' => 'Доступ к объектам недвижимости'
            ],
            'daria.freerealestate.delete_estate' => [
                'tab' => 'FreeRealEstate',
                'label' => 'Возможность удалять объекты'
            ],
            'daria.freerealestate.create_estate' => [
                'tab' => 'FreeRealEstate',
                'label' => 'Возможность создавать объекты'
            ],
            'daria.freerealestate.access_properties' => [
                'tab' => 'FreeRealEstate',
                'label' => 'Доступ к свойствам объектов'
            ],
            'daria.freerealestate.access_applications' => [
                'tab' => 'FreeRealEstate',
                'label' => 'Доступ к заявкам'
            ],
            'daria.freerealestate.create_applications' => [
                'tab' => 'FreeRealEstate',
                'label' => 'Возможность создавать заявки'
            ],
            'daria.freerealestate.delete_applications' => [
                'tab' => 'FreeRealEstate',
                'label' => 'Возможность удалять заявки'
            ],
            'daria.freerealestate.access_values' => [
                'tab' => 'FreeRealEstate',
                'label' => 'Доступ к значениям объектов'
            ],
            'daria.freerealestate.access_chats' => [
                'tab' => 'FreeRealEstate',
                'label' => 'Доступ к чатам'
            ],
        ];
    }

    /**
     * Registers backend navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'freerealestate' => [
                'label' => 'Недвижимость',
                'url' => Backend::url('daria/freerealestate/estates'),
                'icon' => 'icon-leaf',
                'permissions' => ['daria.freerealestate.*'],
                'order' => 500,
                'sideMenu' => [
                    'estates' => [
                        'label' => 'Недвижимость',
                        'url' => Backend::url('daria/freerealestate/estates'),
                        'icon' => 'icon-leaf',
                        'permissions' => ['daria.freerealestate.access_estate'],
                        'order' => 500,
                    ],
                    'properties' => [
                        'label' => 'Свойства объектов',
                        'url' => Backend::url('daria/freerealestate/properties'),
                        'icon' => 'icon-leaf',
                        'permissions' => ['daria.freerealestate.access_properties'],
                        'order' => 500,
                    ],
                    'values' => [
                        'label' => 'Значения свойств',
                        'url' => Backend::url('daria/freerealestate/values'),
                        'icon' => 'icon-leaf',
                        'permissions' => ['daria.freerealestate.access_values'],
                        'order' => 500,
                    ],
                    'requests' => [
                        'label' => 'Заказы',
                        'url' => Backend::url('daria/freerealestate/applications'),
                        'icon' => 'icon-leaf',
                        'permissions' => ['daria.freerealestate.access_applications'],
                        'order' => 500,
                    ]
                ]
            ],
            'chats' => [
                'label' => 'Чаты',
                'url' => Backend::url('daria/freerealestate/topics'),
                'icon' => 'icon-leaf',
                'permissions' => ['daria.freerealestate.access_chats'],
                'order' => 500,
                ]
        ];
    }
}
